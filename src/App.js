import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./containers/Layout/Layout";
import Register from './containers/Register/Register';
import Login from './containers/Login/Login';
import Products from './containers/Products/Products';
import NewProduct from './containers/NewProduct/NewProduct'
import Product from './containers/Product/Product';

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/products/new-product" exact component={NewProduct}/>
                    <Route path="/products/:id" exact component={Product}/>
                    <Route path="/" exact component={Products}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/login" component={Login}/>

                </Switch>
            </Layout>
        );
    }
}

export default App;
