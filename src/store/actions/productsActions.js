import axios from '../../axios';

export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_ONE_PRODUCT = 'FETCH_ONE_PRODUCT';
export const FETCH_CATEGORY_PRODUCTS_SUCCESS = 'FETCH_CATEGORY_PRODUCTS_SUCCESS';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';

export const fetchProductsSuccess = products => {
    return {type: FETCH_PRODUCTS_SUCCESS, products};
};

export const fetchProducts = () => {
    return dispatch => {
        return axios.get('/products').then(
            response => dispatch(fetchProductsSuccess(response.data))
        );
    }
};

export const createProductSuccess = () => {
    return {type: CREATE_PRODUCT_SUCCESS};
};

export const createProduct = productData => {
    return dispatch => {
        return axios.post('/products', productData).then(
            response => dispatch(createProductSuccess())
        );
    };
};

export const fetchOneProductSuccess = (product) => {
    return {type: FETCH_ONE_PRODUCT, product}
};

export const fetchProduct = (id) => {
    return dispatch => {
        return axios.get(`/products/${id}`)
            .then(response => {
                dispatch(fetchOneProductSuccess(response.data));
            })
    }
};

export const fetchCategoryProductsSuccess = (data) => {
    return {type: FETCH_CATEGORY_PRODUCTS_SUCCESS, data}
};

export const fetchCategoryProducts = (id) => {
    return dispatch => {
        return axios.get(`/products/category/${id}`)
            .then(response => {
                dispatch(fetchCategoryProductsSuccess(response.data));
            })
    }
};

export const deleteProduct = (id) => {
    return dispatch => {
        return axios.delete(`/products/${id}`).then(response => {
            console.log(response);
        })
    }
};