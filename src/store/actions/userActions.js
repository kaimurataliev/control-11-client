import axios from '../../axios';
import {push} from 'react-router-redux';

export const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
export const REGISTER_USER_FAILURE = "REGISTER_USER_FAILURE";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAILURE = "LOGIN_USER_FAILURE";
export const LOGOUT_USER = 'LOGOUT_USER';

export const registerUserSuccess = () => {
    return {type: REGISTER_USER_SUCCESS}
};

export const registerUserFailure = (error) => {
    return {type: REGISTER_USER_FAILURE, error}
};

export const registerUser = (userData) => {
    return (dispatch) => {
        axios.post('/users', userData)
            .then(
                response => {
                    dispatch(registerUserSuccess());
                    dispatch(push('/'))
                },
                error => {
                    dispatch(registerUserFailure(error.response.data))
                }
            )
    }
};

export const loginUserSuccess = (user) => {
    return {type: LOGIN_USER_SUCCESS, user}
};

export const loginUserFailure = (error) => {
    return {type: LOGIN_USER_FAILURE, error}
};

export const loginUser = (userData) => {
    return (dispatch) => {
        return axios.post('/users/sessions', userData).then(response => {
                dispatch(loginUserSuccess(response.data));
                dispatch(push('/'))
            },
            error => {
                const errorObj = error.response ? error.response.data : {error: "no internet"};
                dispatch(loginUserFailure(errorObj))
            })
    }
};

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};
        axios.delete('/users/sessions', {headers}).then(
            response => {
                dispatch({type: LOGOUT_USER});
                dispatch(push('/'));
            },
            error => {
                console.log('Could not logout');
            }
        );
    }
};