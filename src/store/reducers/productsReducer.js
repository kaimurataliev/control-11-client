import {FETCH_PRODUCTS_SUCCESS, FETCH_ONE_PRODUCT, FETCH_CATEGORY_PRODUCTS_SUCCESS} from '../actions/productsActions';

const initialState = {
    products: {},
    product: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products};

        case FETCH_ONE_PRODUCT:
            return {...state, product: action.product};

        case FETCH_CATEGORY_PRODUCTS_SUCCESS:
            return {...state, products: action.data};

        default:
            return state;
    }
};

export default reducer;