import React from 'react';
import {SplitButton, MenuItem} from 'react-bootstrap';

const NavMenu = props => {
    return (
        props.categories ? <SplitButton title="Categories">
            {props.categories.map((category, index) => {
                return (
                    <MenuItem key={index}
                        title={category.title}
                        onClick={(id) => props.onClick(category._id)}
                    >
                        {category.title}
                    </MenuItem>
                )
            })}
            <MenuItem onClick={props.all}>All Categories</MenuItem>
        </SplitButton> : null
    )
};

export default NavMenu;