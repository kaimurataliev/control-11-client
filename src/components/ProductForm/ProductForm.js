import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";

class ProductForm extends Component {
    state = {
        title: '',
        description: '',
        image: '',
        price: '',
        category: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.createProduct(formData);

        this.setState({
            title: '',
            description: '',
            image: '',
            price: '',
            category: ''
        })
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value, user: this.props.user.user._id
        });
    };

    selectChangeHandler = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            this.props.categories ? <Form horizontal onSubmit={this.submitFormHandler}>
                <FormGroup controlId="productTitle">
                    <Col componentClass={ControlLabel} sm={2}>
                        Title
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="text"
                            required
                            placeholder="Enter product title"
                            name="title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="productPrice">
                    <Col componentClass={ControlLabel} sm={2}>
                        Price
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="number" min="0"
                            required
                            placeholder="Enter product price"
                            name="price"
                            value={this.state.price}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="productDescription">
                    <Col componentClass={ControlLabel} sm={2}>
                        Description
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            componentClass="textarea"
                            placeholder="Enter description"
                            name="description"
                            value={this.state.description}
                            onChange={this.inputChangeHandler}
                            required
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="productImage">
                    <Col componentClass={ControlLabel} sm={2}>
                        Image
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            required
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="productCategories">
                    <Col componentClass={ControlLabel} sm={2}>
                        Category
                    </Col>
                    <Col sm={10}>
                        <FormControl value="select"
                                     name='category'
                                     onChange={this.selectChangeHandler}
                                     componentClass="select"
                                     required
                        >
                            <option>Choose category</option>
                            {this.props.categories.map(category =>
                                <option
                                    key={category._id}
                                    value={category._id}
                                >
                                    {category.title}

                                </option>
                            )}
                        </FormControl>
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form> : null
        );
    }
}

export default ProductForm;
