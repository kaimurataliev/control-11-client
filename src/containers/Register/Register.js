import React, {Component, Fragment} from 'react';
import {PageHeader, Form, FormGroup, Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {registerUser} from "../../store/actions/userActions";
import FormElement from '../../components/UI/FormElement/FormElement';

class Register extends Component {

    state = {
        username: '',
        password: '',
        displayName: '',
        phoneNumber: ''
    };

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = (event) => {
        event.preventDefault();
        this.props.registerUser(this.state);
    };


    render() {
        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="username"
                        title="Username"
                        type="text"
                        value={this.state.username}
                        changeHandler={this.inputChangeHandler}
                        placeholder="Enter username"
                        autoComplete="new-username"
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        type="password"
                        value={this.state.password}
                        changeHandler={this.inputChangeHandler}
                        placeholder="Enter password"
                        autoComplete="new-password"
                    />

                    <FormElement
                        propertyName="displayName"
                        title="DisplayName"
                        type="text"
                        value={this.state.displayName}
                        changeHandler={this.inputChangeHandler}
                        placeholder="Enter display name"
                        autoComplete="display name"
                    />

                    <FormElement
                        propertyName="phoneNumber"
                        title="PhoneNumber"
                        type="text"
                        value={this.state.phoneNumber}
                        changeHandler={this.inputChangeHandler}
                        placeholder="Enter phone number"
                        autoComplete="phone number"
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Register</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>

        )
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => {
    return {
        registerUser: (userData) => dispatch(registerUser(userData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);