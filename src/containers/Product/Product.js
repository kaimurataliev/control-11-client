import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {deleteProduct, fetchProduct} from "../../store/actions/productsActions";
import {Panel, Image, Button} from 'react-bootstrap';

class Product extends Component {

    componentDidMount() {
        const productId = this.props.match.params.id;
        this.props.fetchProduct(productId);
    }

    onDelete = (event) => {
        event.preventDefault();
        const productId = this.props.product._id;
        this.props.deleteProduct(productId)
    };

    render() {
        return (
            this.props.product ?
                <Fragment>
                    <Panel>
                        <Image
                            style={{width: '100px', marginRight: '10px'}}
                            src={'http://localhost:8000/uploads/' + this.props.product.image}
                            thumbnail
                        />
                        <strong style={{marginLeft: '10px'}}>
                            {this.props.product.title}
                        </strong>
                        <span style={{marginLeft: '10px'}}>
                        {this.props.product.description}
                    </span>
                        <span style={{marginLeft: '10px'}}>
                        Category: {this.props.product.category.description}
                    </span>
                        <strong style={{marginLeft: '10px'}}>
                            Price: {this.props.product.price} KGS
                        </strong>
                        <span style={{marginLeft: '10px'}}>
                        Seller: {this.props.product.user.username}
                        </span>
                        <span style={{marginLeft: '10px'}}>
                            Phone: {this.props.product.user.phoneNumber}
                        </span>
                        <Button
                            onClick={(event) => this.onDelete(event)}
                            style={{marginLeft: '10px'}}
                            bsStyle="danger"
                        >
                            Delete item
                        </Button>
                    </Panel>
                </Fragment> : null

        )
    }
}

const mapStateToProps = state => {
    return {
        product: state.products.product,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchProduct: (id) => dispatch(fetchProduct(id)),
        deleteProduct: (id) => dispatch(deleteProduct(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Product)
