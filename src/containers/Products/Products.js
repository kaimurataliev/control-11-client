import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader, Panel, Image} from "react-bootstrap";
import {fetchCategoryProducts, fetchProducts} from "../../store/actions/productsActions";
import {Link} from "react-router-dom";
import NavMenu from '../../components/NavMenu/NavMenu';

class Products extends Component {

    componentDidMount() {
        this.props.fetchProducts();
    }

    render() {
        return (
            <Fragment>

                <NavMenu all={this.props.fetchProducts} categories={this.props.categories}
                         onClick={(id) => this.props.fetchCategoryProducts(id)}/>

                <PageHeader>
                    Products
                    {this.props.user ? <Link to="/products/new-product">
                        <Button bsStyle="primary" className="pull-right">
                            Add product
                        </Button>
                    </Link> : null}
                </PageHeader>

                {this.props.products.product ? this.props.products.product.map((product, index) => {
                    return (
                        <Panel key={index}>
                            <Panel.Body>
                                <Image
                                    style={{width: '100px', marginRight: '10px'}}
                                    src={'http://localhost:8000/uploads/' + product.image}
                                    thumbnail
                                />
                                <Link to={'/products/' + product._id}>
                                    {product.title}
                                </Link>
                                <strong style={{marginLeft: '10px'}}>
                                    {product.price} KGS
                                </strong>
                            </Panel.Body>
                        </Panel>
                    )
                }) : null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        categories: state.products.products.category,
        products: state.products.products,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchProducts: () => dispatch(fetchProducts()),
        fetchCategoryProducts: (id) => dispatch(fetchCategoryProducts(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);