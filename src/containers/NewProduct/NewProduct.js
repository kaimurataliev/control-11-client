import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct} from "../../store/actions/productsActions";

class NewProduct extends Component {
    createProduct = product => {
        this.props.createProduct(product).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <PageHeader>New product</PageHeader>
                <ProductForm user={this.props.user} categories={this.props.categories} createProduct={this.createProduct} />
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.users.user,
        categories: state.products.products.category
    }
};

const mapDispatchToProps = dispatch => {
    return {
        createProduct: product => {
            return dispatch(createProduct(product))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);